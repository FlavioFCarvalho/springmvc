$('#confirmacaoExclusaoModal').on('show.bs.modal', function(event) {
	
	//primeiro pega o botão q disparou o evento.
	var button = $(event.relatedTarget);
	
	//cria variavel para receber o codigo do titulo
	var codigoTitulo = button.data('codigo');
	
	//pegando descrição do titulo
	var descricaoTitulo = button.data('descricao');
	
	//utiliza o modal p pegar o form
	var modal = $(this);
	var form = modal.find('form');
	
	//pega o action titulo do form
	var action = form.data('url-base');
	
	//fazendo um truque p verificar se estar terminando com barra e se não ele add
	if (!action.endsWith('/')) {
		action += '/';
	}
	
	form.attr('action', action + codigoTitulo);
	
	modal.find('.modal-body span').html('Tem certeza que deseja excluir o título <strong>' + descricaoTitulo + '</strong>?');

});	

$(function() {
	$('[rel="tooltip"]').tooltip();
	$('.js-currency').maskMoney({decimal: ',', thousands: '.', allowZero: true});
});
	