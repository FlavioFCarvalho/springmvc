package com.reobotnet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.reobotnet.model.Titulo;

public interface Titulos extends JpaRepository <Titulo, Long>{

}
